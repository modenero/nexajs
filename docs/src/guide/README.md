# Introduction

Nexa began with the idea of creating an experimental blockchain to innovate on the bleeding edge of what Bitcoin could manage.

The original incantation, [NextChain](https://www.nextchain.cash/) is now known as Nexa Coin.
