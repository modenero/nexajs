---
home: true
heroImage: https://nexajs.org/logo.png
tagline: A complete guide to the JavaScript NEXA library for Node.js and browsers.
actionText: Quick Start →
actionLink: /guide/
features:
- title: Group Tokenization
  details: Nexa has advanced token functionality built directly into the protocol by default.
- title: Advanced Smart Contracts
  details: Nexa has deployed "transaction introspection" allowing smart-contracts to query their own code.
- title: Double-spend Proofs
  details: Double-spend proofs allow businesses to offer faster payments while lowering the risks of fraud.
footer: Made by NexaJS contributors with ❤️
---
